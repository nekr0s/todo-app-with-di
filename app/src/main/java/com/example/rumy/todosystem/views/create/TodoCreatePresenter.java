package com.example.rumy.todosystem.views.create;

import com.example.rumy.todosystem.async.base.SchedulerProvider;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.services.base.TodoService;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

public class TodoCreatePresenter implements TodoCreateContracts.Presenter {

    private final TodoService mTodoService;
    private final SchedulerProvider mSchedulerProvider;
    private TodoCreateContracts.View mView;

    @Inject
    public TodoCreatePresenter(TodoService mTodoService, SchedulerProvider mSchedulerProvider) {
        this.mTodoService = mTodoService;
        this.mSchedulerProvider = mSchedulerProvider;
    }

    @Override
    public void subscribe(TodoCreateContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

    @Override
    public void save(Todo todo) {
        mView.showLoading();
        Disposable disposable = Observable
                .create((ObservableOnSubscribe<Todo>) emitter -> {
                    Todo createdTodo = mTodoService.createTodo(todo);
                    emitter.onNext(createdTodo);
                    emitter.onComplete();
                })
                .subscribeOn(mSchedulerProvider.background())
                .observeOn(mSchedulerProvider.ui())
                .doOnEach(x -> mView.hideLoading())
                .doOnError(mView::showError)
                .subscribe(s -> mView.navigateToHome());

    }
}
