package com.example.rumy.todosystem.diconfig.modules;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.example.rumy.todosystem.views.list.TodoAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewsModule {
    @Provides
    public RecyclerView.Adapter<TodoAdapter.TodoCards> todoCardsAdapter(Context context) {
        return new TodoAdapter();
    }
}
