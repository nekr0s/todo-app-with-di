package com.example.rumy.todosystem.diconfig.modules;

import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.validators.TodoValidator;
import com.example.rumy.todosystem.validators.base.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ValidatorsModule {
    @Provides
    @Singleton
    public Validator<Todo> todoValidator() {
        return new TodoValidator();
    }
}
