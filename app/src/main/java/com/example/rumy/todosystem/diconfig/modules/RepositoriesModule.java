package com.example.rumy.todosystem.diconfig.modules;

import com.example.rumy.todosystem.http.base.HttpRequester;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.parsers.base.JsonParser;
import com.example.rumy.todosystem.repositories.HttpRepository;
import com.example.rumy.todosystem.repositories.base.Repository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoriesModule {
    @Provides
    @Singleton
    public Repository<Todo> todoRepository(
            @Named("baseServerUrl") String baseServerUrl,
            HttpRequester httpRequester,
            JsonParser<Todo> jsonParser
    ) {
        String url = baseServerUrl + "/todos";
        return new HttpRepository<>(httpRequester, url, jsonParser);
    }
}
