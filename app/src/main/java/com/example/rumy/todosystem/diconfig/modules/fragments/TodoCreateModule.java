package com.example.rumy.todosystem.diconfig.modules.fragments;

import com.example.rumy.todosystem.diconfig.ActivityScoped;
import com.example.rumy.todosystem.diconfig.FragmentScoped;
import com.example.rumy.todosystem.views.create.TodoCreateContracts;
import com.example.rumy.todosystem.views.create.TodoCreateFragment;
import com.example.rumy.todosystem.views.create.TodoCreatePresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TodoCreateModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract TodoCreateFragment todoCreateFragment();

    @ActivityScoped
    @Binds
    abstract TodoCreateContracts.Presenter todoCreatePresenter(TodoCreatePresenter presenter);
}
