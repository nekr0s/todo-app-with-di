package com.example.rumy.todosystem.views.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.views.BaseDrawerActivity;
import com.example.rumy.todosystem.R;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class TodoDetailsActivity extends BaseDrawerActivity {

    public static final String EXTRA_KEY = "NAVIGATE_EXTRA";
    private Toolbar mToolbar;

    @Inject
    TodoDetailsFragment mTodoDetailFragment;

    @Inject
    TodoDetailsContracts.Presenter mTodoDetailsPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_details);

        ButterKnife.bind(this);

        mToolbar = findViewById(R.id.drawer_toolbar);

        Intent intent = getIntent();
        Todo todo = (Todo) intent.getSerializableExtra(TodoDetailsActivity.EXTRA_KEY);

        mTodoDetailsPresenter.setTodoId(todo.getId());
        mTodoDetailFragment.setPresenter(mTodoDetailsPresenter);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mTodoDetailFragment)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return 0;
    }

    @Override
    protected Toolbar getDrawerToolbar() {
        return mToolbar;
    }
}
