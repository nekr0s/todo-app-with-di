package com.example.rumy.todosystem.repositories;

import com.example.rumy.todosystem.http.base.HttpRequester;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.parsers.base.JsonParser;
import com.example.rumy.todosystem.repositories.base.Repository;

import java.io.IOException;
import java.util.List;

public class HttpRepository<T> implements Repository<T> {

    private final HttpRequester mHttpRequester;
    private final String mServerUrl;
    private final JsonParser<T> mJsonParser;

    public HttpRepository(HttpRequester mHttpRequester, String mServerUrl, JsonParser<T> mJsonParser) {
        this.mHttpRequester = mHttpRequester;
        this.mServerUrl = mServerUrl;
        this.mJsonParser = mJsonParser;
    }

    @Override
    public List<T> getAll() throws IOException {
        String jsonArray = null;
        jsonArray = mHttpRequester.get(mServerUrl);
        return mJsonParser.fromJsonArray(jsonArray);
    }

    @Override
    public T add(T item) throws IOException {
        String requestBody = mJsonParser.toJson(item);
        String responseBody = mHttpRequester.post(mServerUrl, requestBody);
        return mJsonParser.fromJson(responseBody);
    }

    @Override
    public T getById(int id) throws IOException {
        String url = mServerUrl + "/" + id;
        String json = mHttpRequester.get(url);
        return mJsonParser.fromJson(json);
    }

    @Override
    public void update(int id, T item) throws IOException {
        String url = mServerUrl + "/" + id;
        String json = mJsonParser.toJson(item);
        mHttpRequester.put(url, json);
    }
}
