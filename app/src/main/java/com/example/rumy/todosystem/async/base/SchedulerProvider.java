package com.example.rumy.todosystem.async.base;

import io.reactivex.Scheduler;

public interface SchedulerProvider {
    Scheduler background();

    Scheduler ui();
}
