package com.example.rumy.todosystem.views.create;

import com.example.rumy.todosystem.models.Todo;

public interface TodoCreateContracts {
    interface View {

        void setPresenter(Presenter presenter);

        void navigateToHome();

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(View view);

        void unsubscribe();

        void save(Todo todo);
    }

    public interface Navigator {

        void navigateToHome();
    }

}
