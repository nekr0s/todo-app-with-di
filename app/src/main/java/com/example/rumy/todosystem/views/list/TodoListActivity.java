package com.example.rumy.todosystem.views.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.views.BaseDrawerActivity;
import com.example.rumy.todosystem.views.details.TodoDetailsActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class TodoListActivity extends BaseDrawerActivity implements TodoListContracts.Navigator {

    public static final long IDENTIFIER = 69;
    private Toolbar mToolbar;

    @Inject
    TodoListFragment mTodoListFragment;

    @Inject
    TodoListContracts.Presenter mTodoListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        ButterKnife.bind(this);

        mToolbar = findViewById(R.id.drawer_toolbar);

        mTodoListFragment.setNavigator(this);
        mTodoListFragment.setPresenter(mTodoListPresenter);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mTodoListFragment)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    protected Toolbar getDrawerToolbar() {
        return mToolbar;
    }

    @Override
    public void navigateWith(Todo todo) {
        Intent intent = new Intent(this, TodoDetailsActivity.class);
        intent.putExtra(TodoDetailsActivity.EXTRA_KEY, todo);
        startActivity(intent);
    }
}
