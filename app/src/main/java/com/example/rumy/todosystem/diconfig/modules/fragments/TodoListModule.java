package com.example.rumy.todosystem.diconfig.modules.fragments;

import com.example.rumy.todosystem.diconfig.ActivityScoped;
import com.example.rumy.todosystem.diconfig.FragmentScoped;
import com.example.rumy.todosystem.views.list.TodoListContracts;
import com.example.rumy.todosystem.views.list.TodoListFragment;
import com.example.rumy.todosystem.views.list.TodoListPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TodoListModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract TodoListFragment todoListFragment();

    @ActivityScoped
    @Binds
    abstract TodoListContracts.Presenter todoListPresenter(TodoListPresenter presenter);
}
