package com.example.rumy.todosystem.diconfig;

import com.example.rumy.todosystem.diconfig.modules.fragments.TodoCreateModule;
import com.example.rumy.todosystem.diconfig.modules.fragments.TodoDetailsModule;
import com.example.rumy.todosystem.diconfig.modules.fragments.TodoListModule;
import com.example.rumy.todosystem.views.create.TodoCreateActivity;
import com.example.rumy.todosystem.views.details.TodoDetailsActivity;
import com.example.rumy.todosystem.views.list.TodoListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = TodoListModule.class)
    abstract TodoListActivity todoListActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TodoDetailsModule.class)
    abstract TodoDetailsActivity todoDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TodoCreateModule.class)
    abstract TodoCreateActivity todoCreateActivity();
}
