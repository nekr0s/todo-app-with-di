package com.example.rumy.todosystem.diconfig.modules;

import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.repositories.base.Repository;
import com.example.rumy.todosystem.services.HttpTodoService;
import com.example.rumy.todosystem.services.base.TodoService;
import com.example.rumy.todosystem.validators.base.Validator;

import dagger.Module;
import dagger.Provides;

@Module
public class ServicesModule {
    @Provides
    public TodoService todoService(Repository<Todo> repository, Validator<Todo> validator) {
        return new HttpTodoService(repository, validator);
    }
}
