package com.example.rumy.todosystem.models;

import java.io.Serializable;

public class Todo implements Serializable {

    public String title;
    public String description;
    public boolean status;
    public int id;

    public Todo() {
        // keep empty
    }

    public Todo(String title, String description, boolean status) {
        this.title = title;
        this.description = description;
        this.status = status;
    }

    public Todo(int id, String title, String description, boolean status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
