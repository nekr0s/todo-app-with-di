package com.example.rumy.todosystem.repositories.base;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collector;

public interface Repository<T> {

    List<T> getAll() throws IOException;

    T add(T item) throws IOException;

    T getById(int id) throws IOException;

    void update(int id, T item) throws IOException;
}