package com.example.rumy.todosystem.diconfig;


import android.app.Application;

import com.example.rumy.todosystem.AndroidApplication;
import com.example.rumy.todosystem.diconfig.modules.AsyncModule;
import com.example.rumy.todosystem.diconfig.modules.HttpModule;
import com.example.rumy.todosystem.diconfig.modules.ParsersModule;
import com.example.rumy.todosystem.diconfig.modules.RepositoriesModule;
import com.example.rumy.todosystem.diconfig.modules.ServicesModule;
import com.example.rumy.todosystem.diconfig.modules.ValidatorsModule;
import com.example.rumy.todosystem.diconfig.modules.ViewsModule;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;


@Singleton
@Component(modules = {
        AppModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class,
        ParsersModule.class,
        HttpModule.class,
        RepositoriesModule.class,
        ServicesModule.class,
        ValidatorsModule.class,
        AsyncModule.class,
        ViewsModule.class
})
public interface AppComponent extends AndroidInjector<AndroidApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}