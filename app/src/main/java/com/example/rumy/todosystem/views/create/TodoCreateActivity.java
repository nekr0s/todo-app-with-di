package com.example.rumy.todosystem.views.create;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.rumy.todosystem.views.BaseDrawerActivity;
import com.example.rumy.todosystem.R;
import com.example.rumy.todosystem.views.list.TodoListActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TodoCreateActivity extends BaseDrawerActivity implements TodoCreateContracts.Navigator {

    public static final long IDENTIFIER = 2;

    @BindView(R.id.drawer_toolbar)
    Toolbar mToolbar;

    @Inject
    TodoCreateFragment mTodoCreateFragment;

    @Inject
    TodoCreateContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_create);
        ButterKnife.bind(this);

        mTodoCreateFragment.setPresenter(mPresenter);
        mTodoCreateFragment.setNavigator(this);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mTodoCreateFragment)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    protected Toolbar getDrawerToolbar() {
        return mToolbar;
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(this, TodoListActivity.class);
        startActivity(intent);
        finish();
    }
}

