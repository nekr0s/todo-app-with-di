package com.example.rumy.todosystem.views.list;

import com.example.rumy.todosystem.async.base.SchedulerProvider;
import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.services.base.TodoService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

public class TodoListPresenter implements TodoListContracts.Presenter {

    private final TodoService mTodoService;
    private final SchedulerProvider mSchedulerProvider;
    private TodoListContracts.View mView;

    @Inject
    public TodoListPresenter(TodoService mTodoService, SchedulerProvider mSchedulerProvider) {
        this.mTodoService = mTodoService;
        this.mSchedulerProvider = mSchedulerProvider;
    }


    @Override
    public void subscribe(TodoListContracts.View view) {
        mView = view;
    }

    @Override
    public void loadTodos() {
        mView.showLoading();
        Disposable disposable = Observable
                .create((ObservableOnSubscribe<List<Todo>>) emitter -> {
                    List<Todo> todos = mTodoService.getAllTodos();
                    emitter.onNext(todos);
                    emitter.onComplete();
                })
                .subscribeOn(mSchedulerProvider.background())
                .observeOn(mSchedulerProvider.ui())
                .doFinally(mView::hideLoading)
                .subscribe(this::presentTodosToView,
                        error -> mView.showError(error));
    }

    private void presentTodosToView(List<Todo> todos) {
        if (todos.isEmpty()) {
            mView.showEmptyTodoList();
        } else {
            mView.showTodos(todos);
        }
    }

    @Override
    public void filterTodos(String pattern) {
        mView.showLoading();
        Disposable observable = Observable
                .create((ObservableOnSubscribe<List<Todo>>) emitter -> {
                    List<Todo> todos = mTodoService.getFilteredTodos(pattern);
                    emitter.onNext(todos);
                    emitter.onComplete();
                })
                .subscribeOn(mSchedulerProvider.background())
                .observeOn(mSchedulerProvider.ui())
                .doFinally(mView::hideLoading)
                .subscribe(this::presentTodosToView,
                        error -> mView.showError(error));
    }

    @Override
    public void selectTodo(Todo todo) {
        mView.showTodoDetails(todo);
    }
}
