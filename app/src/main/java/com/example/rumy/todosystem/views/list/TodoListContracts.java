package com.example.rumy.todosystem.views.list;

import com.example.rumy.todosystem.models.Todo;

import java.util.List;

public interface TodoListContracts {
    interface View {
        void setPresenter(Presenter presenter);

        void showTodos(List<Todo> todos);

        void showEmptyTodoList();

        void showError(Throwable throwable);

        void showLoading();

        void hideLoading();

        void showTodoDetails(Todo todo);
    }

    interface Presenter {
        void subscribe(View view);

        void loadTodos();

        void filterTodos(String pattern);

        void selectTodo(Todo todo);
    }

    interface Navigator {
        void navigateWith(Todo todo);
    }
}
