package com.example.rumy.todosystem.views;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.rumy.todosystem.views.create.TodoCreateActivity;
import com.example.rumy.todosystem.views.list.TodoListActivity;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseDrawerActivity extends DaggerAppCompatActivity {


    private void setupDrawer() {

        //create the drawer and remember the `Drawer` result object
        new DrawerBuilder()
                .withActivity(this)
                .withToolbar(getDrawerToolbar())
                .addDrawerItems(
                        new PrimaryDrawerItem().withIdentifier(TodoCreateActivity.IDENTIFIER)
                                .withName("Create todo").withIcon(android.R.drawable.btn_minus),
                        new PrimaryDrawerItem().withIdentifier(TodoListActivity.IDENTIFIER)
                                .withName("Todos list").withIcon(android.R.drawable.btn_minus)
                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    long identifier = drawerItem.getIdentifier();

                    if (getIdentifier() == identifier) return false;

                    Intent intent = getNextIntent(identifier);
                    if (intent == null) return false;

                    startActivity(intent);
                    return true;
                })
                .build();
    }

    private Intent getNextIntent(long identifier) {
        if (identifier == TodoListActivity.IDENTIFIER) {
            return new Intent(BaseDrawerActivity.this, TodoListActivity.class);
        } else if (identifier == TodoCreateActivity.IDENTIFIER) {
            return new Intent(BaseDrawerActivity.this, TodoCreateActivity.class);
        }

        return null;
    }


    protected abstract long getIdentifier();

    protected abstract Toolbar getDrawerToolbar();

    @Override
    protected void onStart() {
        super.onStart();
        setupDrawer();
    }

}
