package com.example.rumy.todosystem.services.base;

import com.example.rumy.todosystem.models.Todo;

import java.io.IOException;
import java.util.List;

public interface TodoService {
    List<Todo> getAllTodos() throws IOException;

    Todo getDetailsById(int id) throws Exception;

    List<Todo> getFilteredTodos(String pattern) throws Exception;

    Todo createTodo(Todo todo) throws Exception;

    void updateTodo(int id, Todo todo) throws Exception;
}
