package com.example.rumy.todosystem;

import com.example.rumy.todosystem.models.Todo;
import com.example.rumy.todosystem.repositories.base.Repository;
import com.example.rumy.todosystem.services.HttpTodoService;
import com.example.rumy.todosystem.validators.base.Validator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTests {
    @Mock
    Repository mockRepository;

    @Mock
    Validator mockValidator;

    @InjectMocks
    HttpTodoService service;

    private List<Todo> defaultTodos = Arrays.asList(
            new Todo(1, "Title1", "Description1", true),
            new Todo(2, "Title2", "Description2", true),
            new Todo(3, "Title2", "Description2", true),
            new Todo(4, "Title3", "Description3", false)
    );

    private Todo defaultInvalidTodo = new Todo(10, "a", "b", false);
    private Todo defaultValidTodo = new Todo(10, "Title1", "Desc2", true);


    @Test
    public void filterTodos_should_returnCurrectTodos_when_thereAreAny() throws Exception {
        Mockito.when(mockRepository.getAll())
                .thenReturn(defaultTodos);

        List<Todo> result = service.getFilteredTodos("Title2");

        int size = result.size();
        Assert.assertEquals(2, size);
        Assert.assertEquals("Title2", result.get(0).getTitle());
        Assert.assertEquals("Title2", result.get(1).getTitle());

    }

    @Test
    public void filterTodos_should_returnEmptyList_when_thereAreNone() throws Exception {
        Mockito.when(mockRepository.getAll())
                .thenReturn(defaultTodos);

        List<Todo> result = service.getFilteredTodos("qqq");

        Assert.assertEquals(0, result.size());
    }

    // Create

    @Test(expected = IllegalArgumentException.class)
    public void createTodo_should_throwException_when_invalidData() throws Exception {
        Mockito.when(mockValidator.isValid(defaultInvalidTodo))
                .thenReturn(false);
        service.createTodo(defaultInvalidTodo);
    }

    @Test
    public void createTodo_should_notThrowException_when_dataIsValid() throws Exception {
        Mockito.when(mockValidator.isValid(defaultValidTodo))
                .thenReturn(true);
        service.createTodo(defaultValidTodo);
    }

}
