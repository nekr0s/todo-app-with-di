# TodoSystem
TodoSystem

Project purpose:

    - Create new ToDo, get from user title and description

    - List all ToDos.

    - View chosen ToDo and change his status.
    
# Project upgrade:
- now uses DI (Butterknife, Dagger)
- Spring server
- Recycler View with Staggered Grid Layout
- ReactiveX for the async tasks.
